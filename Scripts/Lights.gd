extends Area2D

##############
#   Screen   #
##############
# The bottom right corner of the screen.
var screenPosition = Vector2()

################
#   Distance   #
################
# The minimum distance the light needs to be from its previous position. Set programatically later.
var minimumLightDistance = 0.0
# The divisor to calculate the minimum distance based on the screen size.
const LIGHT_DISTANCE_DIVISOR = 5.0

#############
#   Stars   #
#############
# Array to store all of the constellation stars for position referencing.
var constellationStars = []
# Array index of the current star being pointed to.
var currentStar = 0

#######################
#   Collision Timer   #
#######################
# The current time before collision allowing for collision detection again.
var currentCollisionTimer = 0.0
# The time when the collision timer is over.
const MAX_COLLISION_TIMER = 1.0

######################
#   Movement Timer   #
######################
# Maximum movement timer range numbers.
# Lowest time the movement timer can be set for.
const LOW_MOVEMENT_TIMER = 2.0
# Highest time the movement timer can be set for.
const HIGH_MOVEMENT_TIMER = 4.0
# The current time before moving the light.
var currentMovementTimer = 0.0
# The time when the movement timer is over. Will be set randomly every time the light moves.
var maximumMovementTimer = 2.0

##################
#   Star Timer   #
##################
# The current time the ctar is being shined on by the light beam.
var currentStarTimer = 0.0
# The maximum time the star can be hit by the beam before taking damage.
var maximumStarTimer = 3.0
# Check if the player isn't blocking the light beam.
var starDamage = true

#################
#   Targeting   #
#################
# The object that is colliding with the light. Player or shield.
var target = null
var hitPosition1 = Vector2()
var hitPosition2 = Vector2()
var hitPosition3 = Vector2()
var player = null
var playerPos = null
var playerNode = null

func _ready():
	player = get_tree().get_nodes_in_group("player")
	for players in player:
		player = players.get_node('CollisionShape2D').shape.points
	playerPos = get_tree().get_nodes_in_group("player")
	for players in playerPos:
		playerPos = players
		playerNode = players
	# Get the size of the view port window. Bottom right corner position.
	screenPosition = get_viewport_rect().size

	# Set the minimum distance the light has to move from the previous position.
	minimumLightDistance = screenPosition.x / LIGHT_DISTANCE_DIVISOR

	# Set the position of the light beam to be at the bottom right corner of the screen.
	position = screenPosition

	# Form an array with all the constellation's stars that are currently in the game.
	constellationStars = get_tree().get_nodes_in_group("star")

	# Create a random seed. For random number generation.
	randomize()

	# Set the timer to a random number.
	set_new_movement_timer()

	# Move the light and rotate it to a star.
	move_light()	

	# Activate the process.
	set_process(true)
	
	# Activate the physics process.
	set_physics_process(true)

func _process(delta):
	
	hitPosition1 = 0.5 * Vector2(player[0].x, player[0].y)
	hitPosition2 = 0.5 * Vector2(player[1].x, player[1].y)
	hitPosition3 = 0.5 * Vector2(player[2].x, player[2].y)
	hitPosition2.rotated(-playerPos.rotation)
	hitPosition3.rotated(-playerPos.rotation)
	var playerRotation = playerPos.rotation
	var newX = hitPosition1.x * cos(playerRotation) - hitPosition1.y * -sin(playerRotation)
	var newY = hitPosition1.x * sin(playerRotation) - hitPosition1.y * cos(playerRotation)
	hitPosition1.x = newX
	hitPosition1.y = newY
	hitPosition1 += playerPos.position
	hitPosition2 += playerPos.position
	hitPosition3 += playerPos.position
	
	# Increase the movement timer.
	currentMovementTimer += delta
	# Move the light if the timer is done.
	if currentMovementTimer >= maximumMovementTimer:
		# Reset the movement timer.
		currentMovementTimer = 0.0
		# Set a new movement timer time limit.
		set_new_movement_timer()
		# Move the light.
		move_light()
		print(constellationStars[currentStar].name)
	# Used for drawing.
	update()

func _physics_process(delta):
	# Increase the collision timer.
	currentCollisionTimer += delta
	# If the timer is done, collisions can happen again.
	if currentCollisionTimer > MAX_COLLISION_TIMER:
		check_collision()
		# The player is colliding and is the target.
		if target:
			# Raycast to the player for shield collision detection.
			aim()
	# Only happens when the player is not colliding with the light.
	if starDamage:
		if currentStarTimer > maximumStarTimer:
			currentStarTimer = 0.0
			damage_constellation()
		else:
			currentStarTimer += delta

# Check if the player is in front of the light and isn't blocked by anything.
func aim():
	# Get a snapshop of the physics state in the current frame.
	var spaceState = get_world_2d().direct_space_state
	
#	if target.name == "Player":
#		vertices = target.get_node('CollisionShape2D').shape.extents - Vector2(5.0, 5.0)
#
#		for vertex in vertices:
#			hitPosition = 0.5 * Vector2(vertex.x, vertex.y)
		
	# Cast a ray from the light to the target.
	var result = spaceState.intersect_ray(position, playerNode.position, [self], collision_mask)
	
	if result:
		if result.collider.name == "Player" and target:
			result.collider.get_node("PlayerHealth").take_damage()
			result.collider.get_node("Ship").take_damage()
			#print("RayCast Player")
		elif result.collider.name == "Shield" and target:
			currentStarTimer = 0.0
			#print("RayCast Shield")

# Check for collisions.
func check_collision():
	# Create an array with every physics body this is colliding with.
	var collisionBody = get_overlapping_bodies()

	# No collisions were detected.
	if not collisionBody:
		target = null
		starDamage = true
		return

	starDamage = false
	
	# Loop through all the collision bodies that were collided with.
	for body in collisionBody:
		# Check for a collision with the player.
		if body.is_in_group("player"):
			target = body
			currentCollisionTimer = 0.0

	return

# Move the light and rotate it to face a star.
func move_light():
	# Move the light to a new position.
	set_random_x_position()
	# Designate a new star to point to.
	set_random_star()
	# Rotate the light to face the current star.
	rotation_degrees += (get_angle_to(constellationStars[currentStar].position) * 180 / PI)
	return

# Set the array index to a random star.
func set_random_star():
	# Set a temporary variable to make sure the new star is not the same as the current one.
	var previousStar = currentStar
	# Keep getting a random star until it is different from the current one.
	while currentStar == previousStar:
		currentStar = floor(rand_range(0, constellationStars.size()))
	return

# Set a random x position to move the light to.
func set_random_x_position():
	# Store the current position to prevent constant movement.
	var currentPosition = position.x
	# Keep track of the last x position.
	var previousPosition = currentPosition
	# Keep setting a random position until it is further than the minimum distance from the previous position.
	while abs(currentPosition - previousPosition) < minimumLightDistance:
		currentPosition = rand_range(0.0, screenPosition.x)
	# Move the light to the new position.
	position.x = currentPosition
	return

# Set the new maximum for the movement timer to be random.
func set_new_movement_timer():
	maximumMovementTimer = rand_range(LOW_MOVEMENT_TIMER, HIGH_MOVEMENT_TIMER)
	return

# 2D rotation for vectors.
func rotation_2d(_oldPosition, _angle):
	var newX = _oldPosition.x * cos(_angle) - _oldPosition.y * -sin(_angle)
	var newY = _oldPosition.x * sin(_angle) - _oldPosition.y * cos(_angle)
	return Vector2(newX, newY)

func damage_constellation():
	var nodes = get_tree().get_nodes_in_group("constellation")
	for node in nodes:
		if node.name == "Constellation":
			node.get_node("ConstellationHealth").take_damage()
	
#	nodes = get_tree().get_nodes_in_group("city_lights")
#	for node in nodes:
#		if node.name == "CityLights":
#			node.take_damage()
	nodes = get_tree().get_nodes_in_group("constellation_glow")
	for node in nodes:
		if node.name == "ConstellationGlow":
			node.take_damage()

# Draw objects to the screen.
func _draw():
	#if target:
	#	draw_line(Vector2(), (target.position - position).rotated(-rotation), Color(1.0, 0.0, 0.0, 1.0))
	draw_line(Vector2(), (playerNode.position - position).rotated(-rotation), Color(1.0, 0.0, 0.0, 1.0))
	#draw_circle((hitPosition1 - position).rotated(-rotation), 2, Color(1.0, 0.0, 0.0, 1.0))
	#draw_line(Vector2(), (hitPosition1 - position).rotated(-rotation), Color(1.0, 0.0, 0.0, 1.0))
	#draw_line(Vector2(), (hitPosition2 - position).rotated(-rotation), Color(1.0, 0.0, 0.0, 1.0))
	#draw_line(Vector2(), (hitPosition3 - position).rotated(-rotation), Color(1.0, 0.0, 0.0, 1.0))