extends HBoxContainer

func initialize(maximum):
	$TextureProgress.max_value = maximum
	$TextureProgress.value = maximum
	
func change_health(_health):
	$TextureProgress.value = _health