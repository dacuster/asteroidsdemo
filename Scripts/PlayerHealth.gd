extends Node

var playerHealth = 0
export(int) var maxHealth = 9

var interfaceNode = null

func _ready():
	playerHealth = maxHealth
	
	for node in get_tree().get_nodes_in_group("interface"):
		if node.name == "Interface":
			interfaceNode = node.get_node("VBoxContainer").get_node("PlayerHealthBar")
			break
	interfaceNode.initialize(maxHealth)

func take_damage():
	playerHealth -= 1
	playerHealth = max(0, playerHealth)
	interfaceNode.change_health(playerHealth)
	if playerHealth == 0.0:
		get_tree().change_scene("res://Scenes/GameOver.tscn")

func getHealth():
	return Vector2(playerHealth, maxHealth)