extends Node

var constellationHealth = 0
export(int) var maxHealth = 9

var interfaceNode = null

func _ready():
	constellationHealth = maxHealth
	
	for node in get_tree().get_nodes_in_group("interface"):
		if node.name == "Interface":
			interfaceNode = node.get_node("VBoxContainer").get_node("ConstellationHealthBar")
			break
	interfaceNode.initialize(maxHealth)

func take_damage():
	constellationHealth -= 1
	constellationHealth = max(0, constellationHealth)
	interfaceNode.change_health(constellationHealth)
	if constellationHealth  == 0:
		get_tree().change_scene("res://Scenes/GameOver.tscn")

func getHealth():
	return Vector2(constellationHealth, maxHealth)