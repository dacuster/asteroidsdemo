extends Sprite

var playerHealthNode = null

func _ready():
	var allNodes = get_tree().get_nodes_in_group("player")
	for node in allNodes:
		if node.name == "Player":
			playerHealthNode = node.get_node("PlayerHealth")

func take_damage():
	var healthVector = playerHealthNode.getHealth()
	var healthRatio = healthVector.x / healthVector.y
	modulate.a = healthRatio