extends KinematicBody2D

# use export for inpector view
export var rotationSpeed = 2.6
export var thrust = 3
export var maxVelocity = 400
export var playerFriction = 0.65
export var deadZone = 0.25

# other variables
var screenSize = Vector2()
var playerRotation = 0
var playerPosition = Vector2()
var playerVelocity = Vector2()
var playerAcceleration = Vector2()

# Called when the object gets added to the scene.
func _ready():	
	# Get the viewport size.
	screenSize = get_viewport_rect().size
	# Set the player position to the middle of the viewport.
	playerPosition = screenSize / 2
	# Apply the position to the player object.
	#position = playerPosition

	# Enable the physics for this object in the game.
	set_physics_process(true)


func _physics_process(delta):
	check_input()

# Check for user input.
func check_input():
	# Prevent accelerated movement from combined joystick and d-pad presses.
	var isMoving = false

# D-Pad controls.
	# Get the values of the d-pad
	var dPadDirectionX = Vector2(Input.is_joy_button_pressed(0, JOY_DPAD_LEFT), Input.is_joy_button_pressed(0, JOY_DPAD_RIGHT))
	var dPadDirectionY = Vector2(Input.is_joy_button_pressed(0, JOY_DPAD_UP), Input.is_joy_button_pressed(0, JOY_DPAD_DOWN))

	# Get a directions from d-pad presses
	var xDirection = dPadDirectionX.y - dPadDirectionX.x
	var yDirection = dPadDirectionY.y - dPadDirectionY.x

	# Check if the d-pad has been pressed.
	if (xDirection or yDirection):
		move(xDirection, yDirection)
		isMoving = true
#END D-Pad controls.

# Left joystick controls.
	# Get the values of the left joystick
	var leftJoystickDirection = Vector2(Input.get_joy_axis(0, JOY_ANALOG_LX), Input.get_joy_axis(0, JOY_ANALOG_LY))

	# Apply deadzones checks for the left joystick.
	if (leftJoystickDirection.x <= deadZone and leftJoystickDirection.x >= -deadZone):
		leftJoystickDirection.x = 0.0
	if (leftJoystickDirection.y <= deadZone and leftJoystickDirection.y >= -deadZone):
		leftJoystickDirection.y = 0.0

	# Check for left joystick movement
	if ((leftJoystickDirection.x != 0.0 or leftJoystickDirection.y != 0.0) and not isMoving):
		move_object(leftJoystickDirection)
#END Left joystick controls.

# Right joystick controls.
	# Get the values of the right joystick.
	var rightJoystickDirection = Vector2(Input.get_joy_axis(0, JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3))

	# Apply deadzone checks for the right joystick.
	if (rightJoystickDirection.x <= deadZone and rightJoystickDirection.x >= -deadZone):
		rightJoystickDirection.x = 0.0
	if (rightJoystickDirection.y <= deadZone and rightJoystickDirection.y >= -deadZone):
		rightJoystickDirection.y = 0.0

	# Check for movement from the right joystick.
	if (rightJoystickDirection.x != 0.0 or rightJoystickDirection.y != 0.0):
		rotate_object(rightJoystickDirection)
#END Right joystick controls.

# Rotate the object.
func rotate_object(_joystickDirection):
	var differenceAngle = angle_difference(rotation_degrees, _joystickDirection.angle() * 180.0 / PI)

	# Rotate based on the angle difference.
	if (differenceAngle > 2.0):
		rotation_degrees -= rotationSpeed
	elif (differenceAngle < -2.0):
		rotation_degrees += rotationSpeed

	# Make sure the rotation doesn't go past 360 degrees.
	rotation_degrees = fmod(rotation_degrees, 360.0)

# Move the object via the joystick.
func move_object(_joystickDirection):
	position += _joystickDirection * thrust

# Move the object via the d-pad
func move(_xDirection, _yDirection):
	position += Vector2(_xDirection, _yDirection) * thrust

# GameMaker angle_difference function.
func angle_difference(angle1, angle2):
	# Returns a value from -180 to 180
	return fmod(fmod(angle1 - angle2, 360.0) + 540.0, 360.0) - 180.0